#include "matlab.hpp"

// Versions of standard libraries you know from C but implemented for
// C++ have "c" prefix (e.g., stdlib.h -> cstdlib).
#include <cstdlib>

int main() {
    int v[] = {1, 2, 3};
    print_vector(v, 3);

    return EXIT_SUCCESS;
}
