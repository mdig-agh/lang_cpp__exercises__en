#include "gtest/gtest.h"

#include "matlab.hpp"

TEST(MatlabVectorTest, createWithSize) {
    MatVect v(2U);

    // The ASSERT_XXX() macro was used, since checking the subsequent test
    //   conditions make sense only when the vector contains (at least) two
    //   coordinates. If the condition is false, the test should be immediately
    //   stopped (as its further execution and, for instance, a call to `v.get_elem(0)`
    //   may throw an exception if the container is empty.
    // The EXPECT_XXX() macro behaves differently: if the condition is false,
    //   it only marks the whole test as "failed", but continue its execution -
    //   what often provide further diagnostic information (e.g., about actual
    //   values of both (!) coordinatesm regardless whether the first one was
    //   correct or not).
    ASSERT_EQ(v.size(), 2U);
    EXPECT_EQ(v.get_elem(0), 0);
    EXPECT_EQ(v.get_elem(1), 0);
}

TEST(MatlabVectorTest, norm) {
    MatVect v(3U);
    v.set_elem(0, 3);
    v.set_elem(1, 4);

    EXPECT_EQ(v.norm(), 5);
}

TEST(MatlabVectorTest, add) {
    MatVect v1(2U);
    v1.set_elem(0, 1);
    v1.set_elem(1, 2);

    MatVect v2(2U);
    v2.set_elem(0, 4);
    v2.set_elem(1, 5);

    MatVect v_sum = add_vectors(v1, v2);

    ASSERT_EQ(v_sum.size(), 2U);
    EXPECT_EQ(v_sum.get_elem(0), 5);
    EXPECT_EQ(v_sum.get_elem(1), 7);
}
