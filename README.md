# README #

## Repository content ##

The repository contains the following directories:

  * `skeletons` – contains directories with skeleton projects of laboratory exercises (to be completed during classes)  
  * `solutions` – contains directories with solutions to the laboratory exercises  
  * `docs` – contains files used to create projects' documentation (e.g., `.puml` files containing UML class diagram descriptions for [PlantUML](http://www.plantuml.com/))

## Configuration ##

In order to be able to build a given project:

  * Download Google Testing Framework (GTF) [archive](https://github.com/google/googletest/releases/) and
  extract it in anywhere on the hard drive.
  * Open _SetCommonConfig.cmake_ file (either in `skeletons` or `solutions` directory, respectively) and make sure that the value of `GTEST_ROOT` variable points to the location of GTF files.
