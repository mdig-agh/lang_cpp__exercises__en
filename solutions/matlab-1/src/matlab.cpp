#include "matlab.hpp"

// Standard library headers you know from C, but which were implemented
// in C++, have "c" prefix. When including them, do not append ".h" suffix.
// For example: stdlib.h -> cstdlib
#include <cstdlib>

// The <iostream> library is used to handle streamed input/output
// (it's roughly an equivalent of <stdio.h> in C).
#include <iostream>

int* add_vectors(int* v1, int* v2, std::size_t n) {
    int* v_sum = (int*) std::malloc(n * sizeof(int));
    if (v_sum == (int*) NULL) {
        return (int*) NULL;
    }

    for (std::size_t i = 0; i < n; i++) {
        v_sum[i] = v1[i] + v2[i];
    }

    return v_sum;
}

void print_vector(int* v, std::size_t n) {
    for (std::size_t i = 0; i < n; i++) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}
